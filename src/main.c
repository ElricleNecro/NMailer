#include <stdlib.h>
#include <string.h>
#include <ncurses.h>

#include "NCursesWindow.h"

/* void clearyx(int y, int x, unsigned int len) { */
	/* int old_x, old_y; */
	/* getyx(stdscr, old_y, old_x); */

	/* for(int i = 0; i < len; ++i) { */
		/* mvprintw(y, x+i, " "); */
	/* } */

	/* move(old_y, old_x); */
/* } */

int main(int argc, char *argv[]) {
	int ch, x, y;

	NCursesWindow screen = create_screen();
	NCursesWindow window = create_window(screen->height / 2 - (5/2), screen->width / 2 - (80/2), 5, 80);

	printw("Hello World!\n");
	printw("LINES=%d, COLUMNS=%d\n", screen->height, screen->width);
	printw("Type any character to see it in bold\n");

	ch = getch();			// Caught a character.
	if( ch == KEY_F(1) ) {
		printw("F1 Key pressed");
	} else {
		printw("The pressed key is ");
		attron(A_BOLD);		// Activate printing in BOLD.
		printw("%c\n", ch);
		attroff(A_BOLD);	// Deactivating printing in BOLD.
	}

	char str[80] = { 0 };
	getyx(screen->win, y, x);
	echo();
	mvgetnstr(screen->height - 1, 0, str, 79);
	move(y, x);
	printw("%s\n", str);

	refresh();			// Print on the screen what we have.

	set_border(window);
	mvwprintw(window->win, window->height / 2, window->width / 2 - strlen(str) / 2, "%s", str);
	wrefresh(window->win);

	getch();			// And wait for a user input to...

	close_window(window);
	close_screen(screen);

	return 0;

	(void)argc;
	(void)argv;
}
