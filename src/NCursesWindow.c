#include "NCursesWindow.h"

NCursesWindow create_window(int y, int x, int height, int width) {
	NCursesWindow win = calloc(1, sizeof(struct _NCursesWindow));

	win->win    = newwin(height, width, y, x);
	win->width  = width;
	win->height = height;

	return win;
}

NCursesWindow create_screen(void) {
	NCursesWindow win = calloc(1, sizeof(struct _NCursesWindow));

	initscr();			// Init the screen.

	win->win    = stdscr;
	win->height = LINES;
	win->width  = COLS;

	raw();				// We caught every input, even ^C, ^Z...
	keypad(win->win, TRUE);		// We want to be able to get F? keys, num-pad, arrow...
	noecho();			// Not sure, it should not print anything while typing if I understand correctly.
					// It is disabling the behavior we see using scanf.

	return win;
}

void set_border(NCursesWindow win) {
	box(win->win, 0, 0);

	wrefresh(win->win);
}

void set_noborder(NCursesWindow win) {
	wborder(win->win, ' ', ' ', ' ',' ',' ',' ',' ',' ');

	wrefresh(win->win);
}

void close_window(NCursesWindow win) {
	delwin(win->win);

	free(win);
}

void close_screen(NCursesWindow win) {
	endwin();

	free(win);
}
