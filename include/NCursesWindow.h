#ifndef NCURSESWINDOW_H_TAK6PJTJ
#define NCURSESWINDOW_H_TAK6PJTJ

#include <stdlib.h>
#include <ncurses.h>

typedef struct _NCursesWindow {
	WINDOW *win;
	int height, width;
} *NCursesWindow;

NCursesWindow create_window(int y, int x, int height, int width);

NCursesWindow create_screen(void);

void set_border(NCursesWindow win);

void set_noborder(NCursesWindow win);

void close_window(NCursesWindow win);

void close_screen(NCursesWindow win);

#endif /* end of include guard: NCURSESWINDOW_H_TAK6PJTJ */
