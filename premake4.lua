newoption(
	{
		trigger="install-prefix",
		value="DIR",
		description="Directory used to install bin, lib and share directory.",
	}
)

if not _OPTIONS["install-prefix"] then
	_OPTIONS["install-prefix"] = os.getenv("HOME") .. "/.local/"
end

solution("nmailer")
	configurations({"debug", "release"})
		buildoptions(
			{
				"-std=c99"
			}
		)

		flags(
			{
				"ExtraWarnings"
			}
		)

		defines(
			{
				"_GNU_SOURCE"
			}
		)

		includedirs(
			{
				"include/",
				".submodule/ParseArgsC/include/"
			}
		)

	configuration("release")
		buildoptions(
			{
				"-O3"
			}
		)

	configuration("debug")
		buildoptions(
			{
				"-g3"
			}
		)
		flags(
			{
				"Symbols"
			}
		)

	-- project("ParseArgsC")
		-- language("C")
		-- kind("SharedLib")

		-- location("build/lib")
		-- targetdir("build/lib")

		-- files(
			-- {
				-- ".submodule/ParseArgsC/src/Parser.c"
			-- }
		-- )

	project("nmailer")
		language("C")
		kind("ConsoleApp")

		location("build/bin")
		targetdir("build/bin")

		files(
			{
				"src/*",
			}
		)

		defines(
			{
				"LUA_COMPAT_APIINTCASTS"
			}
		)

		libdirs(
			{
				"build/lib"
			}
		)

		links(
			{
				-- "ParseArgsC",
				"ncurses"
			}
		)
